package Part2;

import java.io.*;
import java.util.Date;

public class ReaderManager {
    public void basicFileReader() {
        StringBuilder string = new StringBuilder();
        File file = new File("Instruction.pdf");
        try (FileReader br = new FileReader(file)) {
            String s;
            int data = br.read();
            long before = new Date().getTime();
            while (data != -1) {
                string.append(((char) data));
                data = br.read();
            }
            long after = new Date().getTime();
            System.out.println((after - before) + "4mb");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void basicBufferReader(){
        StringBuilder string = new StringBuilder();
        File file = new File("Instruction.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file)))
        {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "4mb");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_1MB(){
        StringBuilder string = new StringBuilder();
        File file = new File("Instruction.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 1024)) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "4mb");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bufferedReader_4MB(){
        StringBuilder string = new StringBuilder();
        File file = new File("Instruction.pdf");
        try(BufferedReader br = new BufferedReader( new FileReader(file), 4096)) {
            String s;
            long before = new Date().getTime();
            while((s = br.readLine()) !=null){
                string.append(s);
            }
            long after = new Date().getTime();
            System.out.println((after-before) + "4mb");
            System.out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
