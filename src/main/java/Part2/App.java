package Part2;

public class App {
    public static void main(String[] args) {

        ReaderManager rmanager = new ReaderManager();
        rmanager.basicBufferReader();
        rmanager.basicFileReader();
        rmanager.bufferedReader_1MB();
        rmanager.bufferedReader_4MB();
    }
}
