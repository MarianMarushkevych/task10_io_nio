package Part1;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable{
    private List <Droid> droids;
    private transient String name;

    public Ship(List<Droid> droids, String name) {
        this.droids = droids;
        this.name = name;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public void setDroids(List<Droid> droids) {
        this.droids = droids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
