package Part1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class App 
{
    private static final String PATH="Part1.txt";
    public static void main( String[] args ) throws ClassNotFoundException
    {
        serializing();
        deserializing();
    }

    private static void serializing()
    {
        List<Droid> droids=new ArrayList<>();
        droids.add(new Droid(100,1));
        droids.add(new Droid(98,2));
        droids.add(new Droid(33,3));
        droids.add(new Droid(10,4));
        droids.add(new Droid(11,5));
        Ship ship=new Ship(droids,"Iphone");
        try (ObjectOutputStream objectOutputStream=new ObjectOutputStream((new FileOutputStream(PATH)))){
            objectOutputStream.writeObject(ship);
        }catch (IOException e){
            System.out.println("Fatal error");
        }
    }

    private static void deserializing() throws ClassNotFoundException{
        try (ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream(PATH))){
            Ship ship=(Ship) objectInputStream.readObject();
            System.out.printf("Droids: %s%nName: %s", ship.getDroids(), ship.getName());
        }catch (IOException e1){
            System.out.println("Fatal error");
        }
    }
}
