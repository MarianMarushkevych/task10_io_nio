package Part1;

import java.io.Serializable;

public class Droid implements Serializable {
    private  int ch; // ch - CHARGE
    private  int level;

    public Droid(int ch, int level){
        this.ch = ch;
        this.level = level;
    }

    public int getCh() {
        return ch;
    }

    public void setCh(int ch) {
        this.ch = ch;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
